#include "wifi.h"
#include <sys/socket.h>
#include <linux/wireless.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



int requestScan(char* iname){

	/* data for scan request */
	struct iw_scan_req sreq;
	memset(&sreq, 0, sizeof(sreq));

	/* struct for ioctl request */
	struct iwreq req;
	memset(&req, 0, sizeof(req));

	/* socket for ioctl */
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	if (sockfd == -1){
		perror("Cannot create datagram socket");
		exit(EXIT_FAILURE);
	}

	sreq.scan_type = IW_SCAN_TYPE_PASSIVE;
	/* scan all channels */
	sreq.num_channels = 0;
	strcpy(req.ifr_name, iname);
	req.u.data.length = sizeof(struct iw_scan_req);
	req.u.data.pointer = &sreq;

	if(ioctl(sockfd, SIOCSIWSCAN, &req) == -1){
		perror("Error performing SIOCSIWSCAN");
		exit(EXIT_FAILURE);
	}
	close(sockfd);
	return 1;
}

int getScanResult(char* iname){

	/* struct for ioctl request */
	struct iwreq req;
	memset(&req, 0, sizeof(req));

	/* struct for stats */
	struct iw_statistics stats;
	memset(&stats, 0, sizeof(stats));

	/* socket for ioctl */
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	strcpy(req.ifr_name, iname);
	req.u.data.pointer = &stats;
	req.u.data.length = sizeof(struct iw_statistics);
	req.u.data.flags = 1;

	if (sockfd == -1){
		perror("Cannot create datagram socket");
		exit(EXIT_FAILURE);
	}

	if(requestScan(iname) != 1){
		perror("Scan error");
	}
	else{
		if(ioctl(sockfd, SIOCGIWSTATS, &req) == -1){
			perror("Error performing SIOCGIWSCAN");
			exit(EXIT_FAILURE);
			close(sockfd);
		}

		close(sockfd);
		printf("Poziom sygnalu %s %d%s.\n",
			(stats.qual.updated & IW_QUAL_DBM ? " (dBm)" :""), (int)stats.qual.level - 256, (stats.qual.updated & IW_QUAL_LEVEL_UPDATED ? " " :""));
	}
	return 1;
}